package models

//TypeAccount struct db
type TypeAccount struct {
	TypeID          int    `json:"type_id"`
	TypeDescription string `json:"type_description"`
}
