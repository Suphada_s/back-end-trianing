package models

import "github.com/dgrijalva/jwt-go"

//TokenClaim model token
type TokenClaim struct {
	Username string
	Name     string
	//Type	 int
	jwt.StandardClaims
}
