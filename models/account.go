package models

import "time"

//Account struct db
type Account struct {
	TaxID             string    `json:"tax_id"`
	Username          string    `json:"username"`
	Password          string    `json:"password"`
	Firstname         string    `json:"firstname"`
	Lastname          string    `json:"lastname"`
	TypeAccountTypeID int       `json:"type_account_type_id"`
	CrDate            time.Time `json:"cr_date"`
	Token             string    `json:"token"`
}
