package middlewares

import (
	"api/test2/keys"
	"api/test2/models"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

// Login middle
func Login() echo.MiddlewareFunc {
	_, public := keys.GetKey()
	return middleware.JWTWithConfig(middleware.JWTConfig{
		TokenLookup:   "header:" + echo.HeaderAuthorization,
		SigningMethod: "RS256",
		AuthScheme:    "Bearer",
		Claims:        &models.TokenClaim{},
		SigningKey:    public,
		ContextKey:    "account",
	})
}
