package routes

import (
	"api/test2/controllers"
	"api/test2/middlewares"

	"github.com/labstack/echo"
)

//Route path
func Route(e *echo.Echo) {
	accounts := e.Group("/accounts")
	accounts.POST("/api-login", controllers.Login)
	accounts.GET("/api-getlist-user/", controllers.GetListUsernameCustomer, middlewares.Login())
	accounts.DELETE("/api-delete-user/:username", controllers.DeleteUser, middlewares.Login())
	accounts.PUT("/api-update-user", controllers.UpdateUser, middlewares.Login())
	accounts.DELETE("/api-update-user", controllers.RevokeToken, middlewares.Login())
	accounts.POST("/api-refresh", controllers.GenAccessFromRefresh, middlewares.Login())
}
