package configs

import (
	"log"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

//SQL variable work db
var SQL *gorm.DB

//ConnectDataBase function connect to database
func ConnectDataBase() {
	connection := "user05:password05@(203.154.58.128:3306)/training05?charset=utf8&parseTime=True&loc=Local"
	db, err := gorm.Open("mysql", connection)
	if err != nil {
		log.Println(err)
	}
	SQL = db
}
