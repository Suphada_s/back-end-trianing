package controllers

import (
	"api/test2/configs"
	"api/test2/models"
	"net/http"
	"fmt"

	"github.com/labstack/echo"
)

var accessToken string

//Login for user login
func Login(c echo.Context) error {
	account := new(models.Account)
	if err := c.Bind(account); err != nil {
		panic(err)
	}
	Password := account.Password
	CheckUsername := CheckUsernamePattern(account.Username)
	CheckPassword := CheckUsernamePattern(Password)
	if CheckUsername == true && CheckPassword == true {
		fmt.Println(CheckPassword)
		err := configs.SQL.Raw("SELECT * FROM accounts WHERE username = ?", account.Username).Scan(account).Error
		if err != nil {
			panic(err)
		} else {
			if account.Username != "" {
				check := CheckPasswordHash(Password, account.Password)
				if check == true {
					accessToken = GenerateAccessToken(account.Username, account.Firstname)
					account.Token = GenerateRefreshToken(account.Username, account.Firstname)
					UpdateToken(account.Token, account.TaxID)
					accountmap := make(map[string]interface{})
					accountmap["username : "] = account.Username
					accountmap["msg : "] = "Login Success " + account.Firstname
					accountmap["access_token : "] = accessToken
					return c.JSON(http.StatusOK, accountmap)
				}
			} else {
				return c.JSON(http.StatusUnauthorized, "dataNotFound")
			}
		}		
	}
	return c.JSON(http.StatusBadRequest, "Invalid Login")
}

//GetListUsernameCustomer for get username customer by superAdmin
func GetListUsernameCustomer(c echo.Context) error {
	header := c.Request().Header["Authorization"]
	Authorization := CheckTypeHeader(header[0][:7])
	if Authorization == true {
		CheckAdmin, _ := GetAdminFromToken(c)
		if CheckAdmin == true {
			accountAdmin := new([]models.Account)
			errAdmin := configs.SQL.Raw("SELECT * FROM accounts WHERE type_account_type_id=2").Scan(accountAdmin).Error
			if errAdmin != nil {
				panic(errAdmin)
			}

			accountCustomer := new([]models.Account)
			errCustomer := configs.SQL.Raw("SELECT * FROM accounts WHERE type_account_type_id=1").Scan(accountCustomer).Error
			if errCustomer != nil {
				panic(errCustomer)
			}
			accountmap := make(map[string]interface{})
			accountmap["super_admin"] = accountAdmin
			accountmap["customer"] = accountCustomer

			return c.JSON(http.StatusOK, accountmap)
		}
	}

	return c.JSON(http.StatusUnauthorized, "Not Admin")
}

//DeleteUser for delete user by superAdmin
func DeleteUser(c echo.Context) error {
	username := c.Param("username")
	header := c.Request().Header["Authorization"]
	Authorization := CheckTypeHeader(header[0][:7])
	if Authorization == true {
		CheckAdmin, _ := GetAdminFromToken(c)
		if CheckAdmin == true {
			err := configs.SQL.Exec("DELETE FROM accounts WHERE username = ?", username).Error
			if err != nil {
				panic(err)
			}
			accountmap := make(map[string]interface{})
			accountmap["username"] = username
			accountmap["msg"] = "Delete success" + username
			return c.JSON(http.StatusOK, accountmap)
		}
	}
	return c.JSON(http.StatusUnauthorized, "Not Admin")
}

//UpdateUser for update user by superAdmin
func UpdateUser(c echo.Context) error {
	header := c.Request().Header["Authorization"]
	Authorization := CheckTypeHeader(header[0][:7])
	account := new(models.Account)
	if err := c.Bind(account); err != nil {
		panic(err)
	}
	fmt.Println("103"+account.Username)
	if Authorization == true {
		CheckAdmin,claim := GetAdminFromToken(c) //ใช้สำหรับเช็ค Token ว่าเป็น Admin หรือไม่
		fmt.Println("106")
		fmt.Println(CheckAdmin)
		fmt.Println("167" + claim.Username)
		if CheckAdmin == true {
			CheckDataTypeID := CheckTypeID(account.Username) //ใช้สำหรับเช็ค Data ที่ใส่เข้ามาว่าเป็น TypeId ขนิดไหน
			if CheckDataTypeID == 2 {
				CheckName := CheckName(account.Username,claim.Username) //เช็คชื่อของ User ว่าตรงกับ data ไหมในกรณี update ของตัวเอง
				if CheckName == true {
					Password,_ := HashPassword(account.Password)
					err := configs.SQL.Exec("UPDATE accounts SET password = ?,firstname=?,lastname=? WHERE username = ?", Password,account.Firstname,account.Lastname, account.Username).Error
					if err != nil {
						panic(err)
					}
					accountmap := make(map[string]interface{})
					accountmap["username"] = account.Username
					accountmap["msg"] = "Update success " + account.Username
					return c.JSON(http.StatusOK, accountmap)					
				}
				return c.JSON(http.StatusBadRequest, "Invalid Data : Not Allow")
			} else if CheckDataTypeID == 1 {
				Password,_ := HashPassword(account.Password)
				err := configs.SQL.Exec("UPDATE accounts SET password = ?,firstname=?,lastname=? WHERE username = ?", Password,account.Firstname,account.Lastname, account.Username).Error
				if err != nil {
					panic(err)
				}
				accountmap := make(map[string]interface{})
				accountmap["username"] = account.Username
				accountmap["msg"] = "Update success " + account.Username
				return c.JSON(http.StatusOK, accountmap)
			}
		} else if CheckAdmin == false{
			fmt.Println("136"+"before checkname")
			fmt.Println("137"+account.Username, claim.Username)
			CheckName := CheckName(account.Username,claim.Username) 
			fmt.Println(CheckName)
			if CheckName == true {
				Password,_ := HashPassword(account.Password)
				err := configs.SQL.Exec("UPDATE accounts SET password = ?,firstname=?,lastname=? WHERE username = ?", Password,account.Firstname,account.Lastname, account.Username).Error
				if err != nil {
					panic(err)
				}
				accountmap := make(map[string]interface{})
				accountmap["username"] = account.Username
				accountmap["msg"] = "Update success " + account.Username
				return c.JSON(http.StatusOK, accountmap)					
			}
			return c.JSON(http.StatusBadRequest, "Invalid Data : SuperAdmin only")
		}
	}
	fmt.Println("before error")
	return c.JSON(http.StatusUnauthorized, "Error")
}

//RevokeToken for clear refreshtoken
func RevokeToken(c echo.Context) error {
	header := c.Request().Header["Authorization"]
	Authorization := CheckTypeHeader(header[0][:7])
	account := new(models.Account)
	if err := c.Bind(account); err != nil {
		panic(err)
	}
	if Authorization == true {
		_, claim := GetAdminFromToken(c)
		err := configs.SQL.Exec("UPDATE accounts SET token = '' WHERE username = ?", claim.Username).Error
		if err != nil {
			panic(err)
		}
	return c.JSON(http.StatusOK, "Clear token Success")
	}
	return c.JSON(http.StatusBadRequest, "Type Error")
}

//GenAccessFromRefresh generate accesstoken from refreshtoken
func GenAccessFromRefresh(c echo.Context) error {
	header := c.Request().Header["Authorization"]
	Authorization := CheckTypeHeader(header[0][:7])
	account := new(models.Account)
	if Authorization == true {
		_,claim := GetAdminFromToken(c) //ใช้สำหรับเช็ค Token ว่าเป็น Admin หรือไม่
		err := configs.SQL.Raw("SELECT * FROM accounts WHERE username = ?", claim.Username).Scan(account).Error
		if err != nil {
			panic(err)
		}
		account.Token = GenerateAccessToken(account.Username,account.Firstname)
		return c.JSON(http.StatusOK, "Token : "+ account.Token)
	}
	return c.JSON(http.StatusBadRequest, "Invalid Data")
}