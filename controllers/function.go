package controllers

import (
	"api/test2/configs"
	"api/test2/models"
	"regexp"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"golang.org/x/crypto/bcrypt"
)

//CheckPasswordHash for check hash password
func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

//HashPassword hash password for account
func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

//UpdateToken for update token to database
func UpdateToken(token string, TaxID string) {
	errToken := configs.SQL.Exec("UPDATE accounts SET token = ? WHERE tax_id = ?", token, TaxID).Error
	if errToken != nil {
		panic(errToken)
	}
}

//CheckTypeHeader for check pattern header bearer
func CheckTypeHeader(head string) bool {
	pattern, _ := regexp.MatchString("Bearer", head)
	return pattern
}

//CheckUsernamePattern for check username pattern
func CheckUsernamePattern(username string) bool {
	pattern, _ := regexp.MatchString("[a-zA-Z0-9]{6,10}", username)
	return pattern
}

//CheckPasswordPattern for check username pattern
func CheckPasswordPattern(password string) bool {
	pattern, _ := regexp.MatchString("[a-zA-Z0-9]{6,10}", password)
	return pattern
}

//GetAdminFromToken for get username from token
func GetAdminFromToken(c echo.Context) (bool, *models.TokenClaim) {
	account := new(models.Account)
	getToken := c.Get("account").(*jwt.Token)
	claim := getToken.Claims.(*models.TokenClaim)
	err := configs.SQL.Raw("SELECT * FROM accounts WHERE username = ?", claim.Username).Scan(account).Error
	if err != nil {
		panic(err)
	}
	if account.TypeAccountTypeID == 2 {
		return true, claim
	} else {
		return false, claim
	}
}

//CheckTypeID for check typeID
func CheckTypeID(username string) int {
	accountCheck := new(models.Account)
	errCheck := configs.SQL.Raw("SELECT type_account_type_id FROM accounts WHERE username=?",username).Scan(accountCheck).Error
	if errCheck != nil {
		panic(errCheck)
	}
	return accountCheck.TypeAccountTypeID
}

//CheckName for check pattern header bearer เช็คชื่อของ User ว่าตรงกับ data ไหมในกรณี update ของตัวเอง
func CheckName(username string,tokenname string) bool {
	pattern, _ := regexp.MatchString(username, tokenname)
	return pattern
}

