package controllers

import (
	"api/test2/keys"
	"api/test2/models"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
)

//GenerateAccessToken gen access token
func GenerateAccessToken(Username string, firstname string) string {
	now := time.Now()
	exp := now.Add(time.Duration(time.Minute * 10))
	claim := jwt.NewWithClaims(jwt.SigningMethodRS256, models.TokenClaim{Username,
		firstname,
		jwt.StandardClaims{
			IssuedAt:  now.Unix(),
			ExpiresAt: exp.Unix(),
		}})
	private, _ := keys.GetKey()
	token, err := claim.SignedString(private)

	if err != nil {
		fmt.Println(err)
		fmt.Println("Gerne")
		panic(err)
	}
	return token
}

//GenerateRefreshToken gen refresh token
func GenerateRefreshToken(Username string, firstname string) string {
	now := time.Now()
	exp := now.Add(time.Duration(time.Hour * 1))
	claim := jwt.NewWithClaims(jwt.SigningMethodRS256, models.TokenClaim{Username,
		firstname,
		jwt.StandardClaims{
			IssuedAt:  now.Unix(),
			ExpiresAt: exp.Unix(),
		}})
	private, _ := keys.GetKey()
	token, err := claim.SignedString(private)
	if err != nil {
		fmt.Println(err)
		fmt.Println("Gerne")
		panic(err)
	}
	return token
}

// Token Type
var (
	ErrInvalidToken = errors.New("token: invalid token")
)

//ValidateToken chck
func ValidateToken(token string) (*models.TokenClaim, error) {
	_, publicKey := keys.GetKey()

	token = strings.TrimSpace(token)
	if token == "" || len(token) < 8 || strings.ToLower(token[:7]) != "bearer " {
		return nil, ErrInvalidToken
	}
	token = strings.TrimSpace(token[7:])

	tok, err := jwt.ParseWithClaims(token, &models.TokenClaim{}, func(token *jwt.Token) (interface{}, error) {
		// Check is token use correct signing method
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		// return secret for this signing method
		return publicKey, nil
	})
	if err != nil {
		return nil, err
	}
	if claims, ok := tok.Claims.(*models.TokenClaim); ok && tok.Valid {
		return claims, nil
	}
	return nil, ErrInvalidToken
}

//CheckToken func
func CheckToken(c echo.Context) error {
	header := c.Request().Header["Authorization"]
	token, err := ValidateToken(header[0])
	if err != nil {
		return c.JSON(http.StatusUnauthorized, err)
	}
	return c.JSON(http.StatusOK, token)
}
