package keys

import (
	"crypto/rsa"
	"io/ioutil"
	"path/filepath"

	jwt "github.com/dgrijalva/jwt-go"
)

//GetKey ดึงคีย์
func GetKey() (*rsa.PrivateKey, *rsa.PublicKey) {
	keyPrivatePath, _ := filepath.Abs("./keys/test")
	key, err := ioutil.ReadFile(keyPrivatePath)
	if err != nil {
		panic(err)
	}
	privateKey, err := jwt.ParseRSAPrivateKeyFromPEM(key)
	if err != nil {
		panic(err)
	}
	keyPublicPath, _ := filepath.Abs("./keys/test.pub")
	key, err = ioutil.ReadFile(keyPublicPath)
	if err != nil {
		panic(err)
	}
	publicKey, err := jwt.ParseRSAPublicKeyFromPEM(key)
	if err != nil {
		panic(err)
	}
	return privateKey, publicKey
}
