package main

import (
	"api/test2/configs"
	"api/test2/routes"

	"github.com/labstack/echo"
)

func main() {
	e := echo.New()
	routes.Route(e)
	configs.ConnectDataBase()
	defer configs.SQL.Close()
	e.Logger.Fatal(e.Start(":1323"))
}
